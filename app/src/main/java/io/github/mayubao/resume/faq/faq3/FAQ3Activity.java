package io.github.mayubao.resume.faq.faq3;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import io.github.mayubao.resume.faq.R;

public class FAQ3Activity extends AppCompatActivity {

    private static final String TAG = FAQ3Activity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq3);

    }

    public void onClick(View view){
        switch (view.getId()){
            case R.id.btn_start_keep_alive_service: {
                Log.i(TAG, "btn_start_keep_alive_service===>>>click");
                startService(new Intent(this, KeepAliveService.class));
                break;
            }

            case R.id.btn_one_pixel_keep_alive: {
                Log.i(TAG, "btn_one_pixel_keep_alive===>>>click");
                startService(new Intent(this, OnePixelService.class));
                break;
            }
            case R.id.btn_notifycation_keep_alive: {
                Log.i(TAG, "btn_notifycation_keep_alive===>>>click");
                startService(new Intent(this, FakeService.class));
                break;
            }

        }
    }
}
/**
 * 1.Service 内部的onStartCommand方法返回START_STICKY参数无法自己拉起Service(已测试-模拟器)
 * 2.OnePixel开启一个像素的方案暂时无法测试得到。(未测试)
 * 当前的真机为Android7.0,没有权限查看oom_adj值;在模拟器上面又无法熄灭屏幕。导致无法测试。
 * 3.Notifycation提权方案测试通过，确实降低了oom_adj的值，返回后台直接使其进程变为后台进程，即oom_adj=2
 * 如果不适用Notifycation提权方案的话，Home键返回桌面查看oom_adj=7。
 */
