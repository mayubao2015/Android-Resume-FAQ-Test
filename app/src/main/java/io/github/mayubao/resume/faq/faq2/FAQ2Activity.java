 package io.github.mayubao.resume.faq.faq2;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import io.github.mayubao.resume.faq.R;

public class FAQ2Activity extends AppCompatActivity {


    private static final String TAG = FAQ2Activity.class.getSimpleName();
    private ServiceConnection serviceConnection = new ServiceConnection(){

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Log.i(TAG,"Remote Service===>>>onServiceConnected");
            mICalAIDL = ICalAIDL.Stub.asInterface(iBinder);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.i(TAG,"Remote Service===>>>onServiceDisconnected");
        }
    };

    ICalAIDL mICalAIDL = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq2);
    }

    public void onClick(View view){
        switch (view.getId()){
            case R.id.btn_start_service:{
                Log.i(TAG, "btn_start_service===>>>click");
                Intent intent = new Intent(this, RemoteService.class);
                bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
                break;
            }
            case R.id.btn_start_client:{
                Log.i(TAG, "btn_unbind_service==>>>click");
                unbindService(serviceConnection);
                break;
            }
            case R.id.btn_test_add:{
                Log.i(TAG, "btn_test_add===>>>click");
                if(mICalAIDL != null){
                    try {
                        showMsg("远程调用8+8 = " +  mICalAIDL.add(8, 8));
                    } catch (RemoteException e) {
                        e.printStackTrace();
                        showMsg("调用异常");
                    }
                }
                break;
            }
            case R.id.btn_test_minus:{
                Log.i(TAG, "btn_test_minus===>>>click");
                if(mICalAIDL != null) {
                    try {
                        showMsg("远程调用7-2 = " + mICalAIDL.minus(7, 2));
                    } catch (RemoteException e) {
                        e.printStackTrace();
                        showMsg("调用异常");
                    }
                }
                break;
            }
        }
    }

    /**
     * 显示Msg
     * @param msg
     */
    private void showMsg(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
