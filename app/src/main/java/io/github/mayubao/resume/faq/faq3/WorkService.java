package io.github.mayubao.resume.faq.faq3;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.RequiresApi;
import android.util.Log;

import io.github.mayubao.resume.faq.R;

/**
 * Notifycation提权真正的Service
 */
public class WorkService extends Service {
    private static final String TAG = "MyLog";

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "work service onCreate");
        // 开启服务前台运行，id与FakeService相同均为1
        Notification.Builder builder = new Notification.Builder(this);
        builder.setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("fake")
                .setContentText("I am fake")
                .setWhen(System.currentTimeMillis());
        startForeground(1, builder.build());
        // 关闭FakeService，关闭Notification
        FakeService.instance.stopSelf();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "work service onDestroy");
        super.onDestroy();
    }
}
