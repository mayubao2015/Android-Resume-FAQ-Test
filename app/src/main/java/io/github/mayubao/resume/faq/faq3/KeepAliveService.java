package io.github.mayubao.resume.faq.faq3;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.util.Log;

/**
 * 保活的Service
 */
public class KeepAliveService extends Service {

    private static final String TAG = KeepAliveService.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "KeepAliveService--->>>onStartCommand");
//        return Service.START_STICKY;
        return Service.START_STICKY_COMPATIBILITY;
    }
}
