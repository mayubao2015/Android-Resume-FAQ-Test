package io.github.mayubao.resume.faq.faq1;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ListViewCompat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import io.github.mayubao.resume.faq.R;

public class FAQ1Activity extends AppCompatActivity {

    public static final String TAG = FAQ1Activity.class.getSimpleName();
    public static final String[] DATAS = new String[]{
        "1.主线程Handler.post()",
        "2.子线程指定Looper的Handler.post()",
        "3.View.post()",
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq1);

        ListViewCompat listView = (ListViewCompat) findViewById(R.id.listView);
        listView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,DATAS));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i(TAG, "onItemClick===>>>" + position);
                switch (position){
                    case 0:
                        testMainThreadHandlerPost();
                        break;
                    case 1:
                        testSubThreadHandlerPost();
                        break;
                    case 2:
                        testViewPost();
                        break;
                    default:
                }
            }
        });

    }

    /**
     * 测试在所在的线程
     */
    public void test(){
        Log.i(TAG, "***>>>" + Thread.currentThread().getName());
    }

    public void test(String tag){
        Log.i(TAG, tag + "***>>>" + Thread.currentThread().getName());
    }

    /**
     * 主线程测试
     */
    public void testMainThread(){
        test();
    }

    /**
     * 测试主线程中创建handler的情况
     */
    Handler handler = new Handler();
    public void testMainThreadHandlerPost(){
        handler.post(new Runnable() {
            @Override
            public void run() {
                test("MainThreadHandlerPost");

                //测试是会引起ANR
//                try {
//                    Thread.sleep(20 * 1000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                Log.i(TAG, "testMainThreadHandlerPost阻塞成功？");
            }
        });
    }

    /**
     * 测试子线程中创建handler的情况
     */
    public void testSubThreadHandlerPost(){
        new SubThread().start();
    }

    /**
     * 测试View.post(Runnable)
     */
    public void testViewPost(){
        this.findViewById(R.id.listView).post(new Runnable() {
            @Override
            public void run() {
                test("ViewPost");

                //测试是会引起ANR
//                try {
//                    Thread.sleep(20 * 1000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                Log.i(TAG, "testViewPost阻塞成功？");
            }
        });
    }


    class SubThread extends Thread{
        Handler sHandler = null;

        @Override
        public void run() {
            Looper.prepare();
            sHandler = new Handler(Looper.myLooper());
            sHandler.post(new Runnable() {
                @Override
                public void run() {
                    test("SubThreadHandlerPost");
                }
            });
            Looper.loop();
        }
    }

    //运行结果：

//            03-23 15:24:37.341 13113-13113/io.github.mayubao.resume.faq I/FAQ1Activity: onItemClick===>>>0
//            03-23 15:24:37.354 13113-13113/io.github.mayubao.resume.faq I/FAQ1Activity: MainThreadHandlerPost***>>>main
//            03-23 15:24:37.767 13113-13113/io.github.mayubao.resume.faq I/FAQ1Activity: onItemClick===>>>1
//            03-23 15:24:37.772 13113-13240/io.github.mayubao.resume.faq I/FAQ1Activity: SubThreadHandlerPost***>>>Thread-5
//            03-23 15:24:38.204 13113-13113/io.github.mayubao.resume.faq I/FAQ1Activity: onItemClick===>>>2
//            03-23 15:24:38.212 13113-13113/io.github.mayubao.resume.faq I/FAQ1Activity: ViewPost***>>>main


    //上面demo的打印结果很好的说明了问题
    /**
     * 1.MainThread中没有指定Looper的Handler中post一个runnable，这个runnable其实最终还是post在主线程中去执行，不可做耗时任务
     * 2.SubThread中指定子线程Looper的Handler中post一个runnable，这个runnable其实最终还是post在子线程中去执行，可做耗时任务
     * 3.View中post一个runnable，这个runnable其实最终还是post在主线程中去执行，不可做耗时任务
     */
}
