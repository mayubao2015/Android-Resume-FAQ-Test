package io.github.mayubao.resume.faq.faq3;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.widget.Toast;

import io.github.mayubao.resume.faq.R;

/**
 * 使用Notifycation来提权
 */
public class FakeService extends Service {

    private static final String TAG = "MyLog";

    public static FakeService instance = null;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Toast.makeText(this, "FakeService---》》》work", Toast.LENGTH_LONG).show();
        Log.d(TAG, "fake service onCreate");
        // 保存实例
        instance = this;
        // 开启服务前台运行
        Notification.Builder builder = new Notification.Builder(this);
        builder.setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("fake")
                .setContentText("I am fake")
                .setWhen(System.currentTimeMillis());
        startForeground(1, builder.build());
        // 开启真正工作的Service
        Intent intent = new Intent(this, WorkService.class);
        startService(intent);
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "fake service onDestroy");
        // 清除实例
        instance = null;
        // 关闭Notification
        stopForeground(true);
        super.onDestroy();
    }
}


//FAKE一个Service 去开启一个前台的Notifycation来获取提权，实际上这个Service是无用的Service，并且开启真正的WorkService
//真正起作用的是WorkService,真正提权的是在WorkService里去执行
//startForeground(1, Notifycation);是真正提权的方法