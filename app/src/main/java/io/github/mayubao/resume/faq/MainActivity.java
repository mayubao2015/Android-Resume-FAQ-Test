package io.github.mayubao.resume.faq;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;

import io.github.mayubao.resume.faq.faq2.FAQ2Activity;
import io.github.mayubao.resume.faq.faq3.FAQ3Activity;
import io.github.mayubao.resume.faq.faq1.FAQ1Activity;
import io.github.mayubao.resume.faq.faq4.FAQ4Activity;

//import io.github.mayubao.resume.faq.q1.FAQ1Activity;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    public static String[] DATAS = new String[]{
        "1.Handler.post(Runnable)与View.post(Runnable)会不会引起ANR的问题",
        "2.AIDL以及常见的分析问题",
        "3.进程保活测试",
        "4.Handler Looper MessageQueue",
//        "",
    };

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.listView);
        ItemAdapter itemAdapter = new ItemAdapter(this, DATAS);
        itemAdapter.setOnItemClickListener(new ItemAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i(TAG, "onItemClick===>>>" + position);
                Intent intent = new Intent();
                switch (position){
                    case 0:
                        intent.setClass(MainActivity.this, FAQ1Activity.class);
                        break;
                    case 1:
                        intent.setClass(MainActivity.this, FAQ2Activity.class);
                        break;
                    case 2:
                        intent.setClass(MainActivity.this, FAQ3Activity.class);
                        break;
                    case 3:
                        intent.setClass(MainActivity.this, FAQ4Activity.class);
                        break;
                    default:
                }
                startActivity(intent);
            }
        });
        listView.setAdapter(itemAdapter);
    }

    //android7.0 ListView存在兼容性问题
    static class ItemAdapter extends BaseAdapter{
        private Context sContext;
        private String[] sDatas;

        public ItemAdapter(Context context, String[] datas) {
            this.sContext = context;
            this.sDatas = datas;
        }

        @Override
        public int getCount() {
            return sDatas.length;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            convertView = LayoutInflater.from(sContext).inflate(R.layout.item_main, null);
            Button button = ((Button) convertView.findViewById(R.id.btn));
            button.setText(sDatas[position]);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(sOnItemClickListener != null){
                        sOnItemClickListener.onItemClick(null, null, position, -1);
                    }
                }
            });
            return convertView;
        }


        private OnItemClickListener sOnItemClickListener;
        private void setOnItemClickListener(OnItemClickListener listener){
            this.sOnItemClickListener = listener;
        }
        public interface OnItemClickListener{
            public void onItemClick(AdapterView<?> parent, View view, int position, long id);
        }

    }


    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();
}
