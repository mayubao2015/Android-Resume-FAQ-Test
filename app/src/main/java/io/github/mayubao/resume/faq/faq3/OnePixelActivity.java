package io.github.mayubao.resume.faq.faq3;

import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

import io.github.mayubao.resume.faq.R;

public class OnePixelActivity extends AppCompatActivity {
    private static final String TAG = OnePixelActivity.class.getSimpleName();

    public static OnePixelActivity instance = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_pixel);
        Window window = getWindow();
        // 放在左上角
        window.setGravity(Gravity.START | Gravity.TOP);
        WindowManager.LayoutParams layoutParams = window.getAttributes();
        // 宽高为1px
        layoutParams.width = 1;
        layoutParams.height = 1;
        // 起始坐标
        layoutParams.x = 0;
        layoutParams.y = 0;
        window.setAttributes(layoutParams);
        instance = this;
        Log.d(TAG, "activity onCreate");
    }

    @Override
    protected void onDestroy() {
        instance = null;
        Log.d(TAG, "activity onDestroy");
        super.onDestroy();
    }
}
