package io.github.mayubao.resume.faq.utils;

import android.widget.Toast;

import io.github.mayubao.resume.faq.AppContext;

/**
 * Created by Administrator on 2018/3/26.
 */

/**
 * Toast工具类
 */
public class ToastUtils {

    /**
     * Taost信息
     * @param msg
     */
    public static void showMsg(String msg){
        Toast.makeText(AppContext.getAppContext(), msg, Toast.LENGTH_SHORT).show();
    }
}
