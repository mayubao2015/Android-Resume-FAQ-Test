package io.github.mayubao.resume.faq.faq4;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.lang.ref.WeakReference;

import io.github.mayubao.resume.faq.R;
import io.github.mayubao.resume.faq.utils.ToastUtils;

/**
 * 对应FAQ4的sample  (解决Handler还有匿名线程内部类引起内存泄漏的问题)
 */
public class FAQ4Activity extends AppCompatActivity {

    private static final String TAG = FAQ4Activity.class.getSimpleName();

    public static final int TYPE_UI_THREAD_MSG = 1;
    public static final int TYPE_SUB_THREAD_MSG = 2;

    /**
     * 主线程中创建的Handler
     */
    Handler handler = new UIThreadHandler();

    private SubHandlerThread mSubThread;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq4);

        initSubThreadHandlerSendMsg();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(handler != null){
            handler.removeMessages(TYPE_UI_THREAD_MSG);
            handler = null;
        }

        if(mSubThread != null && !mSubThread.isInterrupted()){
//            try{
//                mSubThread.getHandler().getLooper().quit();
//                mSubThread.interrupt();
//            }catch (Exception e){
//                ToastUtils.showMsg("mSubThread.interrupt() ==>> Exception");
//            }
            mSubThread.getHandler().getLooper().quitSafely();
            mSubThread = null;
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_send_msg: {
                Log.i(TAG, "btn_send_msg===>>>handler send msg!");
                handler.sendEmptyMessage(TYPE_UI_THREAD_MSG);
                break;
            }
            case R.id.btn_sub_thread_handler_send_msg: {
                Log.i(TAG, "btn_send_msg===>>>handler send msg!");
                //给指定子线程发送消息
                if(mSubThread!= null && mSubThread.getHandler() != null)
                    mSubThread.getHandler().sendEmptyMessage(TYPE_SUB_THREAD_MSG);
                break;
            }

        }
    }

    /**
     * 初始化子线程中创建Handler
     */
    private void initSubThreadHandlerSendMsg() {
        mSubThread = new SubHandlerThread();
        mSubThread.start();
    }



    //避免Handler内存泄漏
    static class UIThreadHandler extends Handler{
        @Override
        public void handleMessage(Message msg) {
            if(msg != null){
                if(msg.what == TYPE_UI_THREAD_MSG){
                    ToastUtils.showMsg("Handler Show handleMessage!");
                    ToastUtils.showMsg("Current Thread is " + Thread.currentThread().getName());
                }
            }
            super.handleMessage(msg);
        }
    }

    //避免内部类持有外部类的引用内存泄漏
    static class SubHandlerThread extends Thread{
        Handler sHandler;

        public Handler getHandler() {
            return sHandler;
        }

        @Override
        public void run() {
            Looper.prepare();
            sHandler = new Handler(){
                    @Override
                    public void handleMessage(Message msg) {
                        if(msg != null){
                            if(msg.what == TYPE_SUB_THREAD_MSG){
                                ToastUtils.showMsg("Handler Show handleMessage!");
                                ToastUtils.showMsg("Current Thread is " + Thread.currentThread().getName());
                            }
                        }
                        super.handleMessage(msg);
                    }
            };
            Looper.loop();
        }
    }

}
