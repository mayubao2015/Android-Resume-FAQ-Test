package io.github.mayubao.resume.faq.faq2;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * 远程服务的实现
 *
 * Created by Administrator on 2018/3/24.
 */
public class RemoteService extends Service {

    private static final String TAG = RemoteService.class.getSimpleName();

    @Override
    public void onCreate() {
        Log.i(TAG, "onCreate--->>>");
        super.onCreate();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG, "onBind--->>>");
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.i(TAG, "onUnbind--->>>");
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "onDestroy--->>>");
        super.onDestroy();
    }

    /**
     * 远程服务处理
     */
    ICalAIDL.Stub mBinder = new ICalAIDL.Stub(){

        @Override
        public void basicTypes(int anInt, long aLong, boolean aBoolean, float aFloat, double aDouble, String aString) throws RemoteException {

        }

        @Override
        public int add(int x, int y) throws RemoteException {
            Log.i(TAG, "Remote Service add--->>>");
            return x + y;
        }

        @Override
        public int minus(int x, int y) throws RemoteException {
            Log.i(TAG, "Remote Service minus--->>>");
            return x - y;
        }
    };

}
