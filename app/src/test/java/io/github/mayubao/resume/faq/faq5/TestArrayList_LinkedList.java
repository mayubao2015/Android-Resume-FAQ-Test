package io.github.mayubao.resume.faq.faq5;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * author：mayubao on 2018/3/27 15:48
 * email ：345269374@qq.com
 */
public class TestArrayList_LinkedList {

    /** ArrayList TAG */
    public static String TAG_ARRAY_LIST = "ArrayList";
    public static String TAG_LINKED_LIST = "LinkedList";

    public static int TEST_COUNT = 1000000;

    @Test
    public void testAdd(){
        final List<String> arrayList = new ArrayList<>();
        final List<String> linkedList = new LinkedList<>();

        new TestData(TAG_ARRAY_LIST + " Add Test"){

            @Override
            public void doSomething() {
                for(int i=0; i < TEST_COUNT; i++){
                    arrayList.add(String.valueOf(i));
                }
            }
        }.run();

        new TestData(TAG_LINKED_LIST + " Add Test"){

            @Override
            public void doSomething() {
                for(int i=0; i < TEST_COUNT; i++){
                    linkedList.add(String.valueOf(i));
                }
            }
        }.run();
    }


    //TODO 无法测试删除
    @Test
    public void testDel(){
        final List<String> arrayList = new ArrayList<>();
        final List<String> linkedList = new LinkedList<>();

        new TestData(TAG_ARRAY_LIST + "  Del Test"){

            @Override
            public void prepare() {
                super.prepare();
                for(int i=0; i < TEST_COUNT; i++){
                    arrayList.add(String.valueOf(i));
                }
            }

            @Override
            public void doSomething() {
                arrayList.remove(10000);
            }
        }.run();

        new TestData(TAG_LINKED_LIST + " Del Test"){
            @Override
            public void prepare() {
                super.prepare();
                for(int i=0; i < TEST_COUNT; i++){
                    linkedList.add(String.valueOf(i));
                }
            }

            @Override
            public void doSomething() {
                linkedList.remove(linkedList.get(10000));
            }
        }.run();
    }


    /**
     * 测试遍历
     */
    @Test
    public void testTraverse(){
        final List<String> arrayList = new ArrayList<>();
        final List<String> linkedList = new LinkedList<>();

        new TestData(TAG_ARRAY_LIST + "  Traverse Test"){

            @Override
            public void prepare() {
                super.prepare();
                for(int i=0; i < TEST_COUNT; i++){
                    arrayList.add(String.valueOf(i));
                }
            }

            @Override
            public void doSomething() {
//                Iterator<String> iterator = null;
//                for(iterator = arrayList.iterator(); iterator.hasNext(); ){
//                    iterator.next();
//                }
                for(int i=0; i < TEST_COUNT; i++){
                    arrayList.get(i);
                }
            }
        }.run();

        new TestData(TAG_LINKED_LIST + " Traverse Test"){
            @Override
            public void prepare() {
                super.prepare();
                for(int i=0; i < TEST_COUNT; i++){
                    linkedList.add(String.valueOf(i));
                }
            }

            @Override
            public void doSomething() {
                Iterator<String> iterator = null;
//                for(iterator = linkedList.iterator(); iterator.hasNext(); ){
//                    iterator.next();
//                }
                for(String s : linkedList){

                }
            }
        }.run();
    }


    //
}
