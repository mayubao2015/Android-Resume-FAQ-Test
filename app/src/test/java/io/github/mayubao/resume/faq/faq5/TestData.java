package io.github.mayubao.resume.faq.faq5;

import java.util.Date;

/**
 * author：mayubao on 2018/3/27 15:39
 * email ：345269374@qq.com
 */
public abstract class TestData {

    /** 测试ArrayList LinkedList*/
    public static int TYPE_ARRAYLIST_LINKEDLIST = 1;

    private String mTitle;

    public TestData(String title) {
        this.mTitle = title;
    }

    /**
     * 前置或者初始化工作
     */
    public void prepare(){

    }

    public abstract void doSomething();


    public void run(){
        prepare();
        long start = System.currentTimeMillis();
        System.out.println(mTitle + "测试开始时间：" + start);
        doSomething();
        long end = System.currentTimeMillis();
        System.out.println(mTitle + "测试结束时间：" + end);

        System.out.println(mTitle + "总共耗时：" + (end- start) + " ms");
    }

}
